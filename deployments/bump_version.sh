#!/bin/bash

# Read the current version from version.txt
current_version=$(cat version.txt)

# Increment the patch version
new_patch=$(( $(echo $current_version | cut -d '.' -f 3) + 1 ))

# Construct the new version
new_version=$(echo $current_version | awk -F '.' -v patch=$new_patch '{$3=patch; print}' OFS='.')

# Print the new version
echo "$new_version"
