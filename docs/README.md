# DevOps Check-In

- [Introduction](#introduction)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Build and Run](#build-and-run)
- [Docker Compose](#docker-compose)
- [CI/CD Pipeline](#cicd-pipeline)

## Introduction

This repository contains a Golang program named pinger. It responds with "hello world" by default and can be configured to ping another server through environment variables.

## Getting Started

### Prerequisites

Make sure you have the following tools installed:

- Go (check with `go version`) # No need to install if you want to run only docker image . but if you want to run standalone program , need to install go . 
- Docker (check with `docker version`)
- Docker Compose (check with `docker-compose version`)
- Git (check with `git -v`)
- Make (check with `make -v`)

### Build and Run

Follow these steps to build and run the application:

1. Clone this repository: `git clone https://gitlab.com/assignment12815146/devops-checkin-client.git`
2. Change into the project directory: `cd devops-checkin-client`
3. Build the binary: `make build`
4. Build the Docker image: `make docker_image`
   sample output as below
   ![Alt text](image.png)
5. Run the Docker container: `make docker_testrun`
   Sample output as below
   ![Alt text](image-1.png)

## Docker Compose

To demonstrate two pinger services that ping each other, run:

   `make testenv`
   Sample output as below
   ![Alt text](image-2.png)

## CI/CD Pipeline

This repository includes a CI/CD pipeline for automating the build and versioning process. The pipeline is triggered on each push to branches or tags.

- The binary and Docker image tarball are artifacts exposed by the CI/CD pipeline.
- The Docker image is tagged with the version

To manually trigger the CI pipeline:
1. Navigate to "CI / CD > Pipelines" in your GitLab repository.
2. Click on "Run Pipeline" and select the branch or tag.
3. Observe the versioning in action as the Docker image is tagged accordingly.
4. Cicd pipeilne  gilab-ci.yml file path as ![Alt text](../.gitlab-ci.yml) 
5. pipeline blueprint as below
![Alt text](image-3.png)

Note: 
We can extend pipeline to support many.  Tipical production grade application deployment as like below. 
  - build
  - unit test
  - static applicaiton security testing
  - Source code compostion analysis
  - Build docker image 
  - test docker image
  - push docker iamge 
  - security scan docker iamges 
  - deploy app on app/web server/kubernetes env etc   
  - Dynamic application securrity testng.. 
  - in between we can use triggers to call otehr pipelines . 
  - we can execute conditional based  step execution by using "Rules". ie few steps can execute at master or other branches.
  ![Alt text](image-4.png)
 